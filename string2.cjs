function string2(testString) {
  if (testString) {
    const result = testString.split(".");
    if (result.length === 4) {
      return result;
    } else {
      return [];
    }
  } else {
    return [];
  }
}

module.exports = string2;
