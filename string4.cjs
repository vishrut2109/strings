function string4(testString) {
  let temp;
  let result = "";
  if (testString) {
    for (let index in testString) {
      temp = testString[index];

      result = result + temp[0].toUpperCase();

      for (let index1 = 1; index1 < temp.length; index1++) {
        result = result + temp[index1].toLowerCase();
      }

      result = result + " ";
    }
    return result;
  } else {
    return [];
  }
}

module.exports = string4;
