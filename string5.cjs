function string5(testArray) {
  let temp;
  let result = "";
  if (testArray && testArray.length !== 0) {
    for (let index = 0; index < testArray.length; index++) {
      result = result + testArray[index];
      result = result + " ";
    }
    return result;
  } else {
    return "";
  }
}

module.exports = string5;
